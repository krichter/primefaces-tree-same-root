package richtercloud.primefaces.tree.same.root;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private List<List<String>> values = new LinkedList<List<String>>(Arrays.asList(new LinkedList<>(Arrays.asList("a", "b")),
            new LinkedList<>(Arrays.asList("c", "d")),
            new LinkedList<>(Arrays.asList("e", "f"))));
    private TreeNode root = null;

    public BackingBean0() {
    }

    public List<List<String>> getValues() {
        return values;
    }

    public void setValues(List<List<String>> values) {
        this.values = values;
    }

    public TreeNode createRoot(List<String> value) {
        if(root == null) {
            System.out.println("createRoot value: "+value);
            root = new DefaultTreeNode(null);
            new DefaultTreeNode(value,
                    root);
        }
        return root;
    }
}
